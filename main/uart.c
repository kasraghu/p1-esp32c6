#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp_log.h"
#include "string.h"

#include "uart.h"
#include "processData.h"
#include "zigbee.h"

#define UART_PORT_NUM UART_NUM_0
#define UART_BUF_SIZE 1024       // bytes
#define TELEGRAM_MAX_LENGTH 4096 // bytes

static const char *TAG = "uart";
Data processed_data;

inline size_t min(size_t a, size_t b) { return a < b ? a : b; }

static void dsmrReadTask(void *arg)
{
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_DEFAULT,
    };
    int intr_alloc_flags = 0;

    ESP_ERROR_CHECK(gpio_set_pull_mode(GPIO_NUM_17, GPIO_PULLUP_ONLY));
    ESP_ERROR_CHECK(uart_driver_install(UART_PORT_NUM, UART_BUF_SIZE * 2, 0, 0, NULL, intr_alloc_flags));
    ESP_ERROR_CHECK(uart_param_config(UART_PORT_NUM, &uart_config));
    ESP_ERROR_CHECK(uart_set_line_inverse(UART_PORT_NUM, UART_SIGNAL_RXD_INV));

    char *data = (char *)malloc(UART_BUF_SIZE);
    char *telegram = (char *)malloc(TELEGRAM_MAX_LENGTH);
    size_t idx = 0;
    bool endOfData = false;

    while (1)
    {
        int len = uart_read_bytes(UART_PORT_NUM, data, (UART_BUF_SIZE - 1), 200 / portTICK_PERIOD_MS);

        if (len > 0)
        {
            data[len] = 0;
            if (idx == 0)
            {
                char *start = strchr(data, '/');
                if (start != NULL)
                {
                    size_t dataLen = strlen(start);
                    strcpy(telegram, start);
                    idx = dataLen;
                }
                else
                {
                    ESP_LOGW(TAG, "No start tag received");
                }
            }
            else
            {
                char *end = strchr(data, '!');
                size_t maxLen = TELEGRAM_MAX_LENGTH - idx - 1;
                if (end == NULL)
                {
                    strncpy(telegram + idx, data, maxLen);
                    idx += min(len, maxLen);
                }
                else
                {
                    strncpy(telegram + idx, data, min(maxLen, end - data + 1));
                    idx += min(maxLen, end - data + 1);
                    endOfData = true;
                }
                telegram[idx] = 0;
                if (idx == TELEGRAM_MAX_LENGTH - 1)
                {
                    ESP_LOGE(TAG, "Buffer full, and no end received.");
                    idx = 0;
                    endOfData = false;
                }
                else if (endOfData)
                {
                    idx = 0;
                    endOfData = false;
                    processData(telegram, &processed_data);
                    xQueueSend(zigbee_queue, (void *)&processed_data.importPower, portMAX_DELAY);
                }
            }
        }
    }
}

void uartInit()
{

    xTaskCreate(dsmrReadTask, "uart_task", 4096, NULL, 10, NULL);

    vTaskDelay(1200 / portTICK_PERIOD_MS);
}
