/*
 * SPDX-FileCopyrightText: 2021-2022 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: CC0-1.0
 *
 * Zigbee HA_color_dimmable_light Example
 *
 * This example code is in the Public Domain (or CC0 licensed, at your option.)
 *
 * Unless required by applicable law or agreed to in writing, this
 * software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 */

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "rgb_led.h"
#include "zigbee.h"
#include "uart.h"

static const char *TAG = "P1 ESP32C6";

void app_main(void)
{

    ESP_LOGI(TAG, "Initializing...");

    zigbee_init();

    zigbee_queue = xQueueCreate(
        /* The number of items the queue can hold. */
        10,
        /* Size of each item is big enough to hold the
        whole structure. */
        sizeof(uint16_t));

    uartInit();

    uint16_t active_power;
    while (1)
    {
        if (xQueueReceive(zigbee_queue, &active_power, portMAX_DELAY) == pdPASS)
        {
            zigbee_set_value(active_power);
        }
    }
}
