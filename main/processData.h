#ifndef _PROCESSDATA_H_
#define _PROCESSDATA_H_

#include "telegram.h"

void processData(const char *telegram, Data *processed_data);

#endif
