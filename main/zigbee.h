#ifndef _ZIGBEE_H_
#define _ZIGBEE_H_

extern QueueHandle_t zigbee_queue;

void zigbee_init();
void zigbee_set_value(uint16_t total_active_power);

#endif
